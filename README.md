## Lidarr

Lidarr is a music collection manager for Usenet and BitTorrent users. It can monitor multiple RSS feeds for new tracks from your favorite artists and will grab, sort and rename them. It can also be configured
to automatically upgrade the quality of files already downloaded when a better quality format becomes available.

## Bootstrap

```shell
bastille bootstrap https://gitlab.com/bastillebsd-templates/lidarr
```

## Usage

```shell
bastille template TARGET bastillebsd-templates/lidarr
```

### Available arguments `--arg NAME=value`
- USERr=lidarr
- GROUP=lidarr
- DATA_DIR=/usr/local/lidarr
- OUT_PORT=8686

For more options edit `/usr/local/bastille/templates/bastillebsd-templates/lidarr/Bastillefile`
